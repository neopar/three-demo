import React, { useRef, useState } from 'react';
import { useFrame } from 'react-three-fiber';

function Box(props) {
	// This reference will give us direct access to the mesh
	const mesh = useRef();

	// Set up state for the hovered and active state
	const [hovered, setHover] = useState(false);
	const [active, setActive] = useState(false);

	// limit setState() calls
	let changed_hovered = false;
	let changed_active = false;

	// Rotate mesh every frame, this is outside of React without overhead
	useFrame(() => {
		mesh.current.rotation.x = mesh.current.rotation.y += 0.01;

		if (changed_hovered !== hovered) {
			setHover(changed_hovered);
		}

		if (changed_active !== active) {
			setActive(changed_active);
		}
	});

	const getRandomArbitrary = (min, max) => {
		return Math.random() * (max - min) + min;
	};

	const x = getRandomArbitrary(2, 3);
	let size = [x, x, x];
	let hovered_size = [x * 2, x * 2, x * 2];

	return (
		<mesh
			{...props}
			ref={mesh}
			scale={active ? hovered_size : size}
			onClick={(e) => (changed_active = !changed_active)}
			onPointerOver={(e) => (changed_hovered = true)}
			onPointerOut={(e) => (changed_hovered = false)}
			// onClick={(e) => setActive(!active)}
			// onPointerOver={(e) => setHover(true)}
			// onPointerOut={(e) => setHover(false)}
		>
			<boxBufferGeometry attach="geometry" args={size} />
			<meshStandardMaterial attach="material" color={hovered ? 'hotpink' : 'orange'} />
		</mesh>
	);
}

export default Box;
