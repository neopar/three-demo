import React from 'react';
import { Canvas } from 'react-three-fiber';
import Box from './Box/Box';
import './App.css';

function App() {
	const boxes = [];

	const getRandomArbitrary = (min, max) => {
		return Math.random() * (max - min) + min;
	};

	const randomPosition = () => {
		// return [getRandomArbitrary(-100, 100), getRandomArbitrary(-100, 100), getRandomArbitrary(-50, 50)];
		return [getRandomArbitrary(-100, 100), getRandomArbitrary(-100, 100), 0];
	};

	for (let i = 0; i < 1000; i++) {
		boxes.push(randomPosition());
	}

	return (
		<div className="App">
			<Canvas
				gl={{ antialias: true, alpha: false }}
				camera={{ position: [0, 0, 150], near: 5, far: 200 }}
				onCreated={({ gl }) => gl.setClearColor('lightgreen')}
			>
				<ambientLight />
				<pointLight position={[10, 10, 10]} />
				{boxes.map((pos) => {
					return <Box position={pos} />;
				})}
			</Canvas>
		</div>
	);
}

export default App;
